<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="model.Post" %>
<%
	Post post = null;
	if(request.getAttribute("post") != null){
		post = (Post) request.getAttribute("post");
	}else{
		post = new Post();
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規投稿</title>
</head>
<body>
	<div class='main'>
		<form action="new-post" method="post">
			件名 : <input type="text" name="subject" value=<%=post.getSubject() %>><br>
			<br>
			<textarea name="contents"></textarea><br><br>
			category : <input type="text" name="category"><br>
			<input type="submit" value="投稿">
		</form>
		<br>
		<p><%=post.getErrorMessage() %></p>
		<br>
		<a href='/WebBoard/home.jsp'>ホームへ戻る</a>
	</div>
</body>
</html>