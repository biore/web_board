package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import model.Post;

/**
 * Servlet implementation class NewPost
 */
@WebServlet(urlPatterns = { "/new-post" })
public class NewPost extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewPost() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/newPost.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		String subject = request.getParameter("subject");
		String contents = request.getParameter("contents");
		String category = request.getParameter("category");
		Post post = new Post(subject, contents, category);

		String errorMessage = "";
		if(StringUtils.isEmpty(post.getSubject())) {
			errorMessage += "・件名を入力してください<br>";
		}

		if(StringUtils.isEmpty(post.getContent())) {
			errorMessage += "・本文を入力してください<br>";
		}
		if(StringUtils.isEmpty(post.getCategory())) {
			errorMessage += "・カテゴリーを入力してください";
		}

		post.setErrorMessage(errorMessage);

		request.setAttribute("post", post);
		request.getRequestDispatcher("/newPost.jsp").forward(request, response);

	}

}
